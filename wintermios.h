#define BRKINT	0x00000100
#define ICRNL	0x00000200
#define INPCK	0x00004000
#define ISTRIP	0x00008000
#define IXON	0x00020000

#define OPOST	0x00000100

#define CS8	    0x00000c00

#define ECHO	0x00000100
#define ECHOE	0x00000200
#define ECHOK	0x00000400
#define ECHONL	0x00000800
#define ICANON	0x00001000
#define IEXTEN	0x00002000
#define ISIG	0x00004000
#define NOFLSH	0x00008000
#define TOSTOP	0x00010000




struct termios
{
	uint c_iflag;
	uint c_oflag;
	uint c_cflag;
	uint c_lflag;
}
